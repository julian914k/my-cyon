﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

/// <summary>
/// 출처 : http://minhyeokism.tistory.com/23
/// </summary>

public class ObjectPool<T> where T : PoolableObject
{
    private int allocateCount;

    public delegate T Initializer();
    private Initializer initializer;

    private Stack<T> objStack;
    public List<T> objList;

    public ObjectPool()
    {
        // default constructor
    }

    public ObjectPool(int ac, Initializer fn)
    {
        allocateCount = ac;
        initializer = fn;
        objStack = new Stack<T>();
        objList = new List<T>();
    }

    public void Allocate()
    {
        for (int index = 0; index < allocateCount; ++index)
        {
            objStack.Push(initializer());
        }
    }

    public T Pop()
    {
        if (objStack.Count <= 0)
        {
            Allocate();
        }

        T obj = objStack.Pop();
        objList.Add(obj);

        obj.gameObject.SetActive(true);

        return obj;
    }

    public void Push(T obj)
    {
        obj.gameObject.SetActive(false);

        objList.Remove(obj);
        objStack.Push(obj);
    }

    public void Dispose()
    {
        if (objStack == null || objList == null)
            return;

        objList.ForEach(obj => objStack.Push(obj));

        while (objStack.Count > 0)
        {
            GameObject.Destroy(objStack.Pop());
        }

        objList.Clear();
        objStack.Clear();
    }
}
