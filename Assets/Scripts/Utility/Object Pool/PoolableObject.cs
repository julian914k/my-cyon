﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class PoolableObject : MonoBehaviour
{
    protected ObjectPool<PoolableObject> pPool;

    public virtual void Initialize(ObjectPool<PoolableObject> pool)
    {
        pPool = pool;

        gameObject.SetActive(false);
    }

    public virtual void Dispose()
    {
        pPool.Push(this);
    }
}

