﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour
{
    public static MapManager Instance = null;
    public GameObject PlayerShip;
    public GameObject Area1;
    public GameObject Area2;
    public GameObject Area3;
    public GameObject Area4;
    private GameObject[] areaArray;
    private float playerShipPostionX;
    private bool moveLeftAreaFlag = false;
    private bool moveRightAreaFlag = false;
    void Awake()
    {
        Debug.Log(Area1.transform.position.x);
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        areaArray = new GameObject[4];
        areaArray[0] = Area1;
        areaArray[1] = Area2;
        areaArray[2] = Area3;
        areaArray[3] = Area4;

        Debug.Log("Area1 X포지션: " + Area1.transform.position.x);
        Debug.Log("Area2 X포지션: " + Area2.transform.position.x);
        Debug.Log("Area3 X포지션: " + Area3.transform.position.x);
        Debug.Log("Area4 X포지션: " + Area4.transform.position.x);
    }
    void Update()
    {
        if (PlayerShip != null)  // 플레이어 Destroy 시 에러 나서 추가함
            playerShipPostionX = PlayerShip.transform.position.x;
        if ((playerShipPostionX - 10) > areaArray[2].transform.position.x)
        {
            moveLeftAreaFlag = true;
            if (moveLeftAreaFlag)
            {
                Debug.Log("왼쪽이동1: " + areaArray[0].transform.position.x);
                areaArray[0].transform.Translate(80, 0, 0);
                Debug.Log("왼쪽이동2: " + areaArray[0].transform.position.x);
                GameObject tempArea = areaArray[0];
                areaArray[0] = areaArray[1];
                areaArray[1] = areaArray[2];
                areaArray[2] = areaArray[3];
                areaArray[3] = tempArea;
                moveLeftAreaFlag = false;
            }
        }
        else if ((playerShipPostionX + 10) < areaArray[1].transform.position.x)
        {
            moveRightAreaFlag = true;
            if (moveRightAreaFlag)
            {
                Debug.Log("오른쪽이동1: " + areaArray[3].transform.position.x);
                areaArray[3].transform.Translate(-80, 0, 0);
                Debug.Log("오른쪽이동2: " + areaArray[3].transform.position.x);
                GameObject tempArea = areaArray[3];
                areaArray[3] = areaArray[2];
                areaArray[2] = areaArray[1];
                areaArray[1] = areaArray[0];
                areaArray[0] = tempArea;
                moveRightAreaFlag = false;
            }
        }
    }
}
