﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ObjectGenrator : MonoBehaviour
{
    public static ObjectGenrator instance;

    //[Header("Enemy")]
    //public GameObject shooter;
    //public GameObject wingde;

    [Header("Ore")]
    public GameObject silverOre;
    public GameObject goldOre;
    public GameObject blueOre;
    public GameObject redOre;

    [Header("Item")]
    public GameObject woodBox;

    //[Header("Enemy Ratios")]
    //public float enemyRatio = 0.3f;
    //public float shooterRatio = 0.5f;
    //public float wingdeRatio = 0.5f;

    [Header("Ore Ratios")]

    [Range(0, 1)]
    public float oreRatio = 0.3f;
    [Range(0, 1)]
    public float silverOreRatio = 0.3f;
    [Range(0, 1)]
    public float goldOreRatio = 0.3f;
    [Range(0, 1)]
    public float blueOreRatio = 0.3f;
    [Range(0, 1)]
    public float redOreRatio = 0.3f;

    [Header("Item Ratios")]
    [Range(0, 1)]
    public float woodBoxRatio = 0.3f;

    public SpawnPoint[] spawnPoints;
    public List<GameObject> objs;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            Initialize();
        }
    }

    public void Initialize()
    {
        spawnPoints = FindObjectsOfType<SpawnPoint>();
        objs = new List<GameObject>();

        Generate();
    }

    public void Generate()
    {
        if (spawnPoints.Length <= 0)
            return;

        if (objs.Any())
        {
            for (int i = 0; i <objs.Count; ++i)
            {
                Destroy(objs[i].gameObject);
            }
            objs.Clear();
        }

        for (int i = 0; i < spawnPoints.Length; ++i)
        {
            GameObject obj = ChooseObject();
            if (obj == null)
                continue;
            GameObject clone = Instantiate(obj);
            //clone.transform.parent = MapManager.Instance.Area1.transform;
            clone.transform.parent = spawnPoints[i].transform.parent;
            clone.transform.position = spawnPoints[i].transform.position;
            objs.Add(clone);
        }
    }

    public GameObject ChooseObject()
    {
        // 적 유닛
        //if (Random.value > enemyRatio)
        //{
        //    if (Random.value > wingdeRatio)
        //        return wingde;
        //    return null;
        //}

        // 광물
        if (Random.value > oreRatio)
        {
            if (Random.value > silverOreRatio)
                return silverOre;
            if (Random.value > blueOreRatio)
                return blueOre;
            if (Random.value > redOreRatio)
                return redOre;
            if (Random.value > goldOreRatio)
                return goldOre;
            return null;
        }

        // 상자
        if (Random.value > woodBoxRatio)
        {
            if (Random.value > woodBoxRatio)
                return woodBox;
            return null;
        }
        return null;
    }
}
