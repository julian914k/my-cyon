﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StoryManager : MonoBehaviour
{
    public GameObject StoryText;
    private int textIndex = 1;
    private ObjectManager storyObject;
    private Text storyText;
    private Color storyTextColor;
    private bool indexFlag = false;

    void Start()
    {
        storyObject = StoryText.GetComponent<ObjectManager>();
        storyText = StoryText.GetComponent<Text>();
        storyTextColor = storyText.color;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (indexFlag)
            {
                storyObject.alpha = 1.0f;
            }
            else
            {
                storyObject.alpha = 0.1f;
            }
        }

        if (storyObject.alpha >= 1.0f)
        {
            storyObject.AllowFadeInDisplay = false;
            storyObject.AllowFadeOutDisPlay = true;
            if (indexFlag == true)
            {
                textIndex++;
                indexFlag = false;
            }
        }

        if (storyObject.alpha <= 0.0f)
        {
            indexFlag = true;
            switch (textIndex)
            {
                case 1:
                    storyText.text = "서기 62338년.";
                    break;
                case 2:
                    storyText.text = "인간은 뛰어난 과학 기술력을 바탕으로 어느 종족보다 번영하였다.";
                    break;
                case 3:
                    storyText.text = "모든 행성은 인간의 것이었고. 인간은 미래의 번영을 의심하지 않았다.";
                    break;
                case 4:
                    storyText.text = "발달한 의료기술은 인간들에게 영생과 다름 없는 수명을 주었고.";
                    break;
                case 5:
                    storyText.text = "그들은 행성의 자원을 이용해 이상적인 세계를 구현하였다.";
                    break;
                case 6:
                    storyText.text = "그리고 722년 후.";
                    break;
                case 7:
                    storyText.text = "우주의 모든 자원은 인간들에 의해 고갈되었다.";
                    break;
                case 8:
                    storyText.text = "단 하나의 미개척 은하를 제외한 채.";
                    break;
                case 9:
                    SceneManager.LoadScene("MainMenu");
                    break;
            }
            storyObject.AllowFadeOutDisPlay = false;
            storyObject.AllowFadeInDisplay = true;
        }
    }

}
