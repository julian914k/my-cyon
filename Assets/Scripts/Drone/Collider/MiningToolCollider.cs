﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MiningToolCollider : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D col)
    {
        Drone.instance.OnPickUp(col);
    }
}
