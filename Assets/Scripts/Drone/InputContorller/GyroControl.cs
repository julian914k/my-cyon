﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GyroControl : MonoBehaviour
{
    private bool gyroEnabled;
    private Gyroscope gyro;
    private GameObject cameraContainer;
    private Quaternion rotation;

    private Transform forward;

    void Start()
    {
        cameraContainer = new GameObject("Camera Container");
        cameraContainer.transform.position = transform.position;
        transform.SetParent(cameraContainer.transform);
        gyroEnabled = EnableGyro();
    }

    private bool EnableGyro()
    {
        if (SystemInfo.supportsGyroscope)
        {
            gyro = Input.gyro;
            gyro.enabled = true;

            // cameraContainer.transform.rotation = Quaternion.Euler(90f, 90f, 0f);
            cameraContainer.transform.rotation = Quaternion.Euler(90f, 90f, 0f);
            rotation = new Quaternion(0, 0, 1, 0);
            return true;
        }
        return false;
    }

    void Update()
    {
        if (gyroEnabled)
        {
            transform.localRotation = gyro.attitude * rotation;
            //transform.Rotate (0,0,transform.rotation.z);

            //Quaternion originalRot = transform.rotation;    
            // transform.rotation = originalRot * Quaternion.AngleAxis(degrees, Vector3.Up);
            //transform.rotation = Quaternion.AngleAxis(30, Vector3.up);
            //Vector3 rot = transform.localEulerAngles;
            //rot.x = 0;
            //transform.localEulerAngles = rot;
            //rot.y = 0;
            //transform.localEulerAngles.Set(rot.x, rot.y, rot.z);

             transform.rotation = Quaternion.Euler(0f, 0f, transform.localRotation.eulerAngles.z);
            //transform.localRotation = Quaternion.Euler(0f, 0f, transform.localRotation.eulerAngles.z);

            // transform.localRotation = gyro.attitude * rotation;
            transform.rotation = Quaternion.Euler(0f, 0f, transform.localRotation.eulerAngles.z);
        }
    }
}
