﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public bool keyboardMovement;

    private void Awake()
    {
        if(keyboardMovement)
        {
            GetComponent<Rigidbody2D>().gravityScale = 0;
        }
    }

    private void Update ()
    {
        if (keyboardMovement)
        {
            if (Input.GetKey(KeyCode.W))
            {
                Drone.instance.Move(Vector2.up);
            }
            if (Input.GetKey(KeyCode.S))
            {
                Drone.instance.Move(Vector2.down);
            }
            if (Input.GetKey(KeyCode.A))
            {
                Drone.instance.Move(Vector2.left);
            }
            if (Input.GetKey(KeyCode.D))
            {
                Drone.instance.Move(Vector2.right);
            }
        }
        else
        {
            Drone.instance.Rotate();
        }
    }
}
