﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum ClawState
{
    None,
    Grab,
    Return
}

public class Drone : MonoBehaviour
{
    public static Drone instance;

    // Fuel Data
    [Header("Fuel")]
    public float fuelAmount;
    public float fuelMax = 100;

    // Fuel Bar
    public Transform fuelBar;
    private Image fuelBarImage;
    private Image[] fuelBarChildImages;
    private Text fuelBarText;
    private float fuelBarWidth;

    // Claw
    [Header("Claw")]
    public Transform claw;
    public int clawLevel;
    public float clawSpeed = 10;
    public float roapLength;
    public Transform returnToPoint;

    private ClawState clawState;
    private Transform currentItem;
    private GameObject prevItem;
    private Transform itemPoint;
    private float maxDepth;

    public GameObject clawLv1;
    public GameObject clawLv2;
    public GameObject clawLv3;

    private Animator clawAnimator;

    // Movement
    [Header("Movement")]
    public float moveSpeed = 5;
    public Transform upPoint;
    public float upForce = 10;
    public float fuelCost = 1;
    public float sensitivity = 20f;
    public float returnSpeed = 20f;
    private Rigidbody2D rg2d;
    private Quaternion returnQuaternion;
    [Range(0, 360)]
    public int rotationLimit = 45;
    public Animator fireEffectAnimator;
    private bool isFire;
    private InputController inputController;

    // 아이템 효과
    [HideInInspector]
    public bool isShield;

    private void Awake()
    {
        if (instance == null)
            instance = this;

        // Fuel
        fuelAmount = fuelMax;
        fuelBarImage = fuelBar.Find("Gage").GetComponent<Image>();
        fuelBarText = fuelBar.Find("Text").GetComponent<Text>();
        fuelBarChildImages = new Image[fuelBar.Find("Gage").childCount];
        fuelBarWidth = fuelBar.GetComponent<RectTransform>().rect.width;
        for (int i = 0; i < fuelBarChildImages.Length; ++i)
        {
            fuelBarChildImages[i] = fuelBar.Find("Gage").GetChild(i).GetComponent<Image>();
        }

        // Mining Tool
        itemPoint = claw.Find("Item Point");
        SetClawLevel(clawLevel);

        // Movement
        returnQuaternion = transform.rotation;
        rg2d = GetComponent<Rigidbody2D>();
        inputController = GetComponent<InputController>();
    }

    private void Update()
    {
        if (fuelAmount <= 0)
            SceneManager.LoadScene("MainMenu");

        // MiningTool
        if (clawState == ClawState.Grab)
            Grab();

        if (clawState == ClawState.Return)
        {
            ReturnClaw();
            if (Vector3.Distance(returnToPoint.position, claw.position) < 0.2f)
                clawState = ClawState.None;
        }

        if (clawState == ClawState.None)
            claw.position = returnToPoint.position;

        if (currentItem != null)
            currentItem.position = itemPoint.position;

        // Movement
        if (isFire)
        {
            UseFuel(fuelCost);
            MoveForward();
        }
        if (inputController != null && !inputController.keyboardMovement)
        {
            float angle = Vector3.Angle(new Vector3(0, 1, 0), (upPoint.position - transform.position).normalized);
            if (angle > rotationLimit)
            {
                transform.rotation = Quaternion.Euler(0, 0, Input.acceleration.x > 0 ? -rotationLimit : rotationLimit);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Item"))
        {
            col.GetComponent<Item>().Use(this);
        }
    }

    // Movement
    public void Move(Vector3 dir)
    {
        transform.position += dir * moveSpeed * Time.deltaTime;
    }

    public void MoveForward()
    {
        if (fuelAmount <= 0)
            return;

        Vector3 forward = (upPoint.position - transform.position).normalized;
        rg2d.AddForce(forward * upForce);
    }

    public void Rotate()
    {
        if (Input.acceleration.x > 0.1f || Input.acceleration.x < -0.1f)
        {
            float angle = Vector3.Angle(new Vector3(0, 1, 0), (upPoint.position - transform.position).normalized);
            if (angle > rotationLimit)
                return;
            transform.Rotate(0, 0, -Input.acceleration.x * sensitivity);
        }
        else
        {
            if (transform.position.x - 0.5f < upPoint.position.x && upPoint.position.x < transform.position.x + 0.5f)
            {
                transform.rotation = returnQuaternion;
                return;
            }

            if (upPoint.position.x > transform.position.x + 0.05f)
            {
                transform.Rotate(0, 0, 10);
            }

            if (upPoint.position.x < transform.position.x - 0.05f)
            {
                transform.Rotate(0, 0, -10);
            }
        }
    }

    public void StartFire()
    {
        isFire = true;

        fireEffectAnimator.gameObject.SetActive(true);
        fireEffectAnimator.SetInteger("Mining Level", clawLevel);
    }

    public void EndFire()
    {
        isFire = false;

        fireEffectAnimator.gameObject.SetActive(false);
    }

    // Fuel
    public void UseFuel(float value)
    {
        fuelAmount -= value;
        if (fuelAmount < 0)
            fuelAmount = 0;
        fuelBarImage.fillAmount = Mathf.Min(fuelAmount, fuelMax) / fuelMax;
        for (int i = 0; i < fuelBarChildImages.Length; ++i)
        {
            fuelBarChildImages[i].fillAmount = fuelBarImage.fillAmount;
        }
    }

    // 아이템 효과
    public void Sheild()
    {
        isShield = true;
    }

    // Mining
    public void SetClawLevel(int lv)
    {
        clawLevel = Mathf.Clamp(lv, 1, 3);

        switch (clawLevel)
        {
            case 1:
                clawLv1.SetActive(true);
                clawLv2.SetActive(false);
                clawLv3.SetActive(false);

                clawAnimator = clawLv1.GetComponent<Animator>();
                break;
            case 2:
                clawLv1.SetActive(false);
                clawLv2.SetActive(true);
                clawLv3.SetActive(false);

                clawAnimator = clawLv2.GetComponent<Animator>();
                break;
            case 3:
                clawLv1.SetActive(false);
                clawLv2.SetActive(false);
                clawLv3.SetActive(true);

                clawAnimator = clawLv3.GetComponent<Animator>();
                break;
        }
    }

    public void DropClaw()
    {
        if (clawState != ClawState.None)
            return;

        if (currentItem != null)
        {
            currentItem.GetComponent<BoxCollider2D>().isTrigger = false;
            prevItem = currentItem.gameObject;
        }
        currentItem = null;
        clawState = ClawState.Grab;
        maxDepth = claw.position.y - roapLength;

        clawAnimator.SetBool("Fold Out", true);
    }

    public void Grab()
    {
        claw.position += Vector3.down * clawSpeed * Time.deltaTime;
        if (claw.position.y <= maxDepth)
        {
            clawState = ClawState.Return;
            maxDepth = 0;

            clawAnimator.SetBool("Fold Out", false);
        }
    }

    public void ReturnClaw()
    {
        claw.position = Vector3.Lerp(claw.position, returnToPoint.position, 0.5f);
    }

    private IEnumerator WaitMiningToolActive()
    {
        yield return new WaitForSeconds(0.5f);

        prevItem = null;
    }

    public void OnPickUp(Collider2D col)
    {
        if (col.CompareTag("Ground"))
        {
            clawState = ClawState.Return;
            maxDepth = 0;

            clawAnimator.SetBool("Fold Out", false);
        }
        if (col.CompareTag("Item") || col.CompareTag("Enemy"))
        {
            if (clawState == ClawState.Grab)
            {
                if (prevItem != null)
                {
                    if (col.gameObject == prevItem)
                    {
                        StartCoroutine(WaitMiningToolActive());
                        return;
                    }
                }
                currentItem = col.transform;
                currentItem.GetComponent<BoxCollider2D>().isTrigger = true;

                clawAnimator.SetBool("Fold Out", false);
                clawState = ClawState.Return;
            }
        }
    }

    public void Die()
    {
        Destroy(transform.parent.gameObject);
    }
}
