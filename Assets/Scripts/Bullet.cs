﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    // PROJECTILE
    public float Speed = 5;
    public float Damage = 1;
    public float LifeTime = 3;
    private Vector3 direction;
    private EnemyAI owner;
    private Transform detectedTarget;
    public static Bullet Create(Bullet instance, EnemyAI owner, Transform launchPoint, Transform detectedTarget, float damage)
    {
        Bullet bullet = GameObject.Instantiate<Bullet>(instance);
        bullet.owner = owner;
        bullet.detectedTarget = detectedTarget;
        bullet.Damage = damage;

        // Set the start position
        Vector2 position = launchPoint.position;
        bullet.transform.position = position;

        // Make sure we can't collide with the person who shot this projectile
        Bullet.IgnoreOwnerCollisions(bullet, owner);

        return bullet;
    }
    void Start()
    {
        // Get rid of the projectile after a while if it doesn't hit anything
        StartCoroutine(this.DestroyAfter(this.LifeTime));

        // Give it some velocity
        direction = (detectedTarget.position - transform.position).normalized;
        //Rigidbody2D body = this.GetComponent<Rigidbody2D>();

        Debug.Log("총알 발사: " + direction);
        Debug.Log("총알 발사2: " + Speed);
        //body.AddForce(new Vector2(500, 0));
        //body.AddForce(direction*500);

    }

    void Update()
    {
        Debug.Log("총알 발사: " + direction);
        Debug.Log("총알 발사2: " + Speed);
        Debug.Log("총알 발사3: " + (direction * Speed * Time.deltaTime));
        transform.position += (direction * Speed * Time.deltaTime);
    }

    void OnTriggeEnter2D(Collider2D other)
    {
            Debug.Log("enter");
        if (other.CompareTag("Player"))
        {
            if (other.GetComponent<Drone>().isShield)
            {
                other.GetComponent<Drone>().isShield = false;
            }
            else
            {
                other.GetComponent<Drone>().UseFuel(Damage);
            }

            GameObject.Destroy(this.gameObject);
        }
    }


    public static void IgnoreOwnerCollisions(Bullet bullet, EnemyAI owner)
    {
        if (owner != null)
        {
            Collider2D[] colliders = owner.GetComponentsInChildren<Collider2D>();
            Collider2D[] projectileColliders = bullet.GetComponentsInChildren<Collider2D>();
            for (int i = 0; i < colliders.Length; i++)
            {
                for (int j = 0; j < projectileColliders.Length; j++)
                {
                    Physics2D.IgnoreCollision(colliders[i], projectileColliders[j]);
                }
            }
        }
    }

    private IEnumerator DestroyAfter(float seconds)
    {
        yield return new WaitForSeconds(seconds);

        GameObject.Destroy(this.gameObject);
    }

}
