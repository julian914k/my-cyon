﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[Serializable]
public class MouseStayButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public UnityEvent onStay;
    public UnityEvent onPointerDown;
    public UnityEvent onPointerUp;

    private bool isStay;

    private void Update()
    {
        if (isStay)
        {
            if(onStay != null)
                onStay.Invoke();
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isStay = true;

        if(onPointerDown != null)
            onPointerDown.Invoke();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isStay = false;

        if (onPointerUp != null)
            onPointerUp.Invoke();
    }
}
