﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwitchButton : MonoBehaviour
{
    public GameObject obj1;
    public GameObject obj2;

    public void Awake()
    {
        Button btn = GetComponent<Button>();

        obj1.SetActive(true);
        obj2.SetActive(false);

        btn.onClick.AddListener(Switch);
    }

    public void Switch()
    {
        obj1.SetActive(!obj1.activeSelf);
        obj2.SetActive(!obj2.activeSelf);
    }
}
