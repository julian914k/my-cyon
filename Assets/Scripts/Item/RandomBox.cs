﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class RandomBox : MonoBehaviour
{
    [Header("Prefab")]
    public GameObject defenceArtifact;
    public GameObject healArtifact;
    public GameObject redore;
    public GameObject blueore;
    public GameObject winde;
    public GameObject shooter;

    public void OnTriggerEnter2D(Collider2D col)
    {
        if(col.GetComponent<MiningToolCollider>() != null)
        {
            float rn = Random.value;
            if(0 < rn && rn <= 15)
            {
                CreateItem(defenceArtifact);
            }
            else if (15 < rn && rn <= 30)
            {
                CreateItem(healArtifact);
            }
            else if (30 < rn && rn <= 35)
            {
                CreateItem(redore);
            }
            else if (35 < rn && rn <= 55)
            {
                CreateItem(defenceArtifact);
            }
            else if (55 < rn && rn <= 75)
            {
                CreateItem(healArtifact);
            }
            else if (75 < rn && rn <= 90)
            {
                CreateItem(winde);
            }
            else
            {
                CreateItem(shooter);
            }

            Broken();
        }
    }

    public void Broken()
    {
        gameObject.SetActive(false);
    }

    public void CreateItem(GameObject obj)
    {
        if (obj != null)
        {
            GameObject create = Instantiate(obj);
            create.transform.position = transform.position;
        }
    }
}
