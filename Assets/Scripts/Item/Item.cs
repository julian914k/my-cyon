﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Item : MonoBehaviour
{
    public abstract void Use(Drone drone);
    public abstract void Use(Basecamp basecamp);
}
