﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public enum OreType
{
    SilverOre,
    GoldOre,
    BlueOre,
    RedOre,
}

public class Ore : Item
{
    public OreType type;

    private int value;

    public void Awake()
    {
        switch(type)
        {
            case OreType.SilverOre:
                value = 35;
                break;
            case OreType.GoldOre:
                value = 80;
                break;
            case OreType.BlueOre:
                value = 150;
                break;
            case OreType.RedOre:
                value = 300;
                break;
        }
    }
    
    public override void Use(Basecamp basecamp)
    {
        basecamp.AddMineral(value);
        Score.Instance.Currentscore += value;
    }

    public override void Use(Drone drone)
    {

    }
}
