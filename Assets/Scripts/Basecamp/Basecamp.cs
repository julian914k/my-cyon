﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Basecamp : MonoBehaviour
{
    public GameObject basecampMenu;
    public LandingSencor landingSencor;

    public Sprite[] models;
    public SpriteRenderer spriteRenderer;

    public Text MiningLevelText;
    public Text AttackLevelText;
    public Text FuelLevelText;

    public Text MineralAmountText;

    public Text BasecampLevelCostText;
    public Text MiningLevelCostText;
    public Text AttackLevelCostText;
    public Text FuelLevelCostText;

    public int BasecampLevel = 1;
    public int MiningLevel = 0;
    public int AttackLevel = 0;
    public int FuelLevel = 0;

    public int MineralAmount = 0;

    public int BasecampLevelCost1 = 500;
    public int BasecampLevelCost2 = 2000;
    public int MiningLevelCost0 = 150;
    public int MiningLevelCost1 = 400;
    public int AttackLevelCost0 = 150;
    public int AttackLevelCost1 = 400;
    public int FuelLevelCost0 = 150;
    public int FuelLevelCost1 = 400;

    void Start()
    {
        BasecampLevelCostText.text = BasecampLevelCost1.ToString();
        MiningLevelCostText.text = MiningLevelCost0.ToString();
        AttackLevelCostText.text = AttackLevelCost0.ToString();
        FuelLevelCostText.text = FuelLevelCost0.ToString();
        MineralAmountText.text = MineralAmount.ToString();

        landingSencor.baseCamp = this;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Item") || col.CompareTag("Enemy"))
        {
            col.GetComponent<Item>().Use(this);

            Destroy(col.gameObject);
        }
    }

    public void AddMineral()
    {
        MineralAmount += 100;
        MineralAmountText.text = MineralAmount.ToString();
    }

    public void CheckBasecampLevelCost()
    {
        switch (BasecampLevel)
        {
            case 1:
                if (CheckUpgradeCost(BasecampLevelCost1))
                {
                    AddBasecampLevel();
                    spriteRenderer.sprite = models[1];
                }
                break;
            case 2:
                if (CheckUpgradeCost(BasecampLevelCost2))
                {
                    AddBasecampLevel();
                    spriteRenderer.sprite = models[2];
                }
                break;
        }
    }

    public void CheckMinigLevelCost()
    {
        if (BasecampLevel <= MiningLevel)
            return;

        switch (MiningLevel)
        {
            case 0:
                if (CheckUpgradeCost(MiningLevelCost0))
                {
                    AddMiningLevel();
                    Drone.instance.SetClawLevel(2);
                }
                break;
            case 1:
                if (CheckUpgradeCost(MiningLevelCost1))
                {
                    AddMiningLevel();
                    Drone.instance.SetClawLevel(3);
                }
                break;
        }
    }
    public void CheckAttackLevelCost()
    {
        if (BasecampLevel <= AttackLevel)
            return;

        switch (AttackLevel)
        {
            case 0:
                if (CheckUpgradeCost(AttackLevelCost0))
                {
                    AddAttackLevel();
                }
                break;
            case 1:
                if (CheckUpgradeCost(AttackLevelCost1))
                {
                    AddAttackLevel();
                }
                break;
        }
    }
    public void CheckFuelLevelCost()
    {
        if (BasecampLevel <= FuelLevel)
            return;

        switch (FuelLevel)
        {
            case 0:
                if (CheckUpgradeCost(FuelLevelCost0))
                {
                    AddFuelLevel();
                    Drone.instance.fuelMax += 20;
                }
                break;
            case 1:
                if (CheckUpgradeCost(FuelLevelCost1))
                {
                    AddFuelLevel();
                    Drone.instance.fuelMax += 50;
                }
                break;
        }
    }
    private bool CheckUpgradeCost(int cost)
    {
        if (MineralAmount >= cost)
        {
            MineralAmount -= cost;
            MineralAmountText.text = MineralAmount.ToString();
            return true;
        }
        // Debug.Log("업그레이드할 광물이 부족합니다!");
        return false;
    }
    private void AddBasecampLevel()
    {
        BasecampLevel++;
        BasecampLevelCostText.text = BasecampLevelCost2.ToString();
    }
    private void AddMiningLevel()
    {
        MiningLevel++;
        MiningLevelCostText.text = MiningLevelCost1.ToString();
        MiningLevelText.text = MiningLevel.ToString();
    }
    private void AddAttackLevel()
    {
        AttackLevel++;
        AttackLevelCostText.text = AttackLevelCost1.ToString();
        AttackLevelText.text = AttackLevel.ToString();
    }
    private void AddFuelLevel()
    {
        FuelLevel++;
        FuelLevelCostText.text = FuelLevelCost1.ToString();
        FuelLevelText.text = FuelLevel.ToString();
    }

    public void AddMineral(int mineralAmount)
    {
        MineralAmount += mineralAmount;
        MineralAmountText.text = MineralAmount.ToString();
    }
    
    public void FullFuel()
    {
        Drone.instance.fuelAmount = Drone.instance.fuelMax;
    }

    public void OnLand()
    {
        FullFuel();
        basecampMenu.SetActive(true);
        ObjectGenrator.instance.Generate();
    }
}
