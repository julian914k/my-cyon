﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LandingSencor : MonoBehaviour
{
    [HideInInspector]
    public Basecamp baseCamp;

	public void OnTriggerEnter2D(Collider2D col)
    {
        if(col.CompareTag("Player"))
        {
            baseCamp.OnLand();
        }
    }

    public void OnTriggerExit2D(Collider2D col)
    {
        if(col.CompareTag("Player"))
            baseCamp.basecampMenu.SetActive(false);
    }
}
