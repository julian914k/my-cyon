﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class StartScene : MonoBehaviour
{
    public GameObject press;
    float temp;

    void Start()
    {
        press.SetActive(false);
    }

    void Update()
    {
        temp += Time.deltaTime;
        if (temp > 0.5f && temp < 1.0f)
        {
            press.SetActive(true);
        }
        else if (temp > 1.8f)
        {
            temp = 0;
            press.SetActive(false);
        }
    }

    public void OnMouseDown()
    {
        Debug.Log("마우스 클릭");
        SceneManager.LoadScene("Story");
    }
}
