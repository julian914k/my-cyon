﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class MainMenuManager : MonoBehaviour
{
    public GameObject MainCamera;
    public AudioClip ClickAudio;

    public Transform Stage1;
    public Transform Stage2;
    public Transform Stage3;    

    private bool allowMoveStage1 = false;
    private bool allowMoveStage2 = false;
    private bool allowMoveStage3 = false;

    public float cameraTrackingSpeed = 0.1f;
    private Vector3 startPosition = Vector3.zero;
    private Vector3 targetPosition = Vector3.zero;

    private int stageNumber;

    public void OnMouseDown()    
    {
        Debug.Log("On Mouse Down");
                SceneManager.LoadScene("Stage1");
    }

    void LateUpdate()
    {
        if (allowMoveStage1)
        {
            startPosition = MainCamera.transform.position;
            MainCamera.transform.position = Vector3.Lerp(startPosition, targetPosition, cameraTrackingSpeed);
            if (MainCamera.transform.position.x == targetPosition.x)
            {
                allowMoveStage1 = false;
            }
        }
        else if (allowMoveStage2)
        {
            startPosition = MainCamera.transform.position;
            MainCamera.transform.position = Vector3.Lerp(startPosition, targetPosition, cameraTrackingSpeed);
            if (MainCamera.transform.position.x == targetPosition.x)
            {
                allowMoveStage1 = false;
            }
        }
        else if (allowMoveStage3)
        {
            startPosition = MainCamera.transform.position;
            MainCamera.transform.position = Vector3.Lerp(startPosition, targetPosition, cameraTrackingSpeed);
            if (MainCamera.transform.position.x == targetPosition.x)
            {
                allowMoveStage1 = false;
            }
        }
    }

    public void MoveStage(int stageNumber)
    {
        switch (stageNumber)
        {
            case 1:
                allowMoveStage1 = true;
                targetPosition.Set(Stage1.transform.position.x, Stage1.transform.position.y, MainCamera.transform.position.z);
                break;
            case 2:
                allowMoveStage2 = true;
                targetPosition.Set(Stage2.transform.position.x, Stage2.transform.position.y, MainCamera.transform.position.z);
                break;
            case 3:
                allowMoveStage3 = true;
                targetPosition.Set(Stage3.transform.position.x, Stage3.transform.position.y, MainCamera.transform.position.z);
                break;
        }
    }

    public void RightSwipe()
    {
        switch (stageNumber)
        {
            case 1:
                stageNumber++;
                MoveStage(stageNumber);
                break;
            case 2:
                stageNumber++;
                MoveStage(stageNumber);
                break;
            case 3:
                stageNumber = 1;
                MoveStage(stageNumber);
                break;
        }
    }
    public void LeftSwipe()
    {
        switch (stageNumber)
        {
            case 1:
                stageNumber = 3;
                MoveStage(stageNumber);
                break;
            case 2:
                stageNumber--;
                MoveStage(stageNumber);
                break;
            case 3:
                stageNumber--;
                MoveStage(stageNumber);
                break;
        }
    }

    private Vector3 fp;   //First touch position
    private Vector3 lp;   //Last touch position
    private float dragDistance;  //minimum distance for a swipe to be registered

    void Start()
    {
        dragDistance = Screen.height * 15 / 100; //dragDistance is 15% height of the screen
    }

    void Update()
    {
        if (Input.touchCount == 1) // user is touching the screen with a single touch
        {
            Touch touch = Input.GetTouch(0); // get the touch
            if (touch.phase == TouchPhase.Began) //check for the first touch
            {
                fp = touch.position;
                lp = touch.position;
            }
            else if (touch.phase == TouchPhase.Moved) // update the last position based on where they moved
            {
                lp = touch.position;
            }
            else if (touch.phase == TouchPhase.Ended) //check if the finger is removed from the screen
            {
                lp = touch.position;  //last touch position. Ommitted if you use list

                //Check if drag distance is greater than 20% of the screen height
                if (Mathf.Abs(lp.x - fp.x) > dragDistance || Mathf.Abs(lp.y - fp.y) > dragDistance)
                {//It's a drag
                 //check if the drag is vertical or horizontal
                    if (Mathf.Abs(lp.x - fp.x) > Mathf.Abs(lp.y - fp.y))
                    {   //If the horizontal movement is greater than the vertical movement...
                        if ((lp.x > fp.x))  //If the movement was to the right)
                        {   //Right swipe
                            Debug.Log("Right Swipe");
                        }
                        else
                        {   //Left swipe
                            Debug.Log("Left Swipe");
                        }
                    }
                    else
                    {   //the vertical movement is greater than the horizontal movement
                        if (lp.y > fp.y)  //If the movement was up
                        {   //Up swipe
                            Debug.Log("Up Swipe");
                        }
                        else
                        {   //Down swipe
                            Debug.Log("Down Swipe");
                        }
                    }
                }
                else
                {   //It's a tap as the drag distance is less than 20% of the screen height
                    Debug.Log("Tap");
                }
            }
        }
    }
}
