﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score
{
    private static Score instance = null;
    public static Score Instance
    {
        get
        {
            if (instance == null)
                instance = new Score();

            return instance;
        }
    }

    private Score()
    {
    }

    public int Currentscore = 0;
    public int Grade = 0;
    public int CurrentLevel = 1;
}