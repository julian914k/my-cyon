﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WingdeAI : EnemyAI
{
    public Bullet Bullet;
    public Transform DetectPoint;
    public LayerMask DetectedTargetLayer;
    public float DetectRadius = 3.0f;
    public Vector2 TraceRange = new Vector2(5.0f, 2.0f);

    public float DelayActionTime = 2;

    public float Speed = 10f;
    public float AttackMinDistance = 3f;

    private bool hasDetectedTarget = false;
    private Transform detectedTarget;
    private Vector2 detectedTargetDistance;

    private bool allowFire = true;
    private float fireCoolTime = 3f;
    private float fireTimer = 0.0f;

    private Rigidbody2D body2D;

    void Start()
    {
        body2D = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        if (HP <= 0) GameObject.Destroy(gameObject);

        // 탐지 레이더 작동
        if (!hasDetectedTarget)
        {
            Detect();
        }

        // 탐지한 타겟 제대로 찾는지 여부 확인하는 로그
        if (hasDetectedTarget)
        {
            // Debug.Log("Detect Collider: " + detectedTarget);
            UpdateTargetInfo();
            CheckTraceRange();
            if (Mathf.Abs(detectedTargetDistance.x) < AttackMinDistance)
            {
                StopMove();
                // 멈춘채로 공격
                if (allowFire)
                {
                    Debug.Log("멈춘채로 공격");
                    Fire();
                }
            }
            else
            {
                Trace();
            }
            fireTimer -= Time.deltaTime;
            if (fireTimer <= 0) allowFire = true;
        }
    }

    public void Fire()
    {
        //Instantiate(Bullet, transform.position, Quaternion.identity);
        Bullet.Create(Bullet, this, transform, detectedTarget, Damage);
        fireTimer = fireCoolTime;
        allowFire = false;

    }

    private void SetDetectedTargetDistance()
    {
        if (detectedTarget != null)
        {
            detectedTargetDistance.x = transform.position.x - detectedTarget.position.x;
            detectedTargetDistance.y = transform.position.y - detectedTarget.position.y;
            //Debug.Log("DetectedTargetDistance: " + detectedTargetDistance.ToString());
        }
    }
    private void ReleaseDetectedTarget()
    {
        //Debug.Log("추적 타겟 해제");
        StopMove();
        detectedTarget = null;
        hasDetectedTarget = false;
    }

    private void Detect()
    {
        Collider2D detectedTargetCollider = Physics2D.OverlapCircle(DetectPoint.position, DetectRadius, DetectedTargetLayer);
        // Debug.Log("디텍트: " + detectedTargetCollider);
        if (detectedTargetCollider != null)
        {
            Debug.Log("추적 타겟 설정");
            detectedTarget = detectedTargetCollider.transform;
            hasDetectedTarget = true;
        }
    }

    protected void UpdateTargetInfo()
    {
        SetDetectedTargetDistance();
    }

    protected void CheckTraceRange()
    {
        if (Mathf.Abs(detectedTargetDistance.x) - TraceRange.x > 0 ||
            Mathf.Abs(detectedTargetDistance.y) - TraceRange.y > 0)
        {
            ReleaseDetectedTarget();
        }
    }

    protected void Trace()
    {
        if (detectedTarget != null)
        {
            transform.position = Vector3.MoveTowards(transform.position, detectedTarget.position, Speed * 0.01f * Time.deltaTime);
        }
    }

    protected void StopMove()
    {
        Debug.Log("멈춤 함수 콜");
        body2D.velocity = Vector2.zero;
    }
}

public enum Direction { Left, Right }
