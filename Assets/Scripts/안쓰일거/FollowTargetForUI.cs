﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTargetForUI : MonoBehaviour
{
    public Transform target;

    private void Update()
    {
        Vector3 screen = Camera.main.WorldToScreenPoint(target.position);
        Vector3 position = Camera.main.ScreenToWorldPoint(screen);

        transform.position = position;
    }
}
