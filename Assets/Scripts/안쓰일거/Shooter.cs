﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    public Projectile projectile;

    public Transform forwardPoint;

    public float shootSpeed = 10;
    public float projectileLifeTime = 5f;

    public ObjectPool<Projectile> pool;

    public void Allocate()
    {
        pool = new ObjectPool<Projectile>(10, () =>
        {
            GameObject obj = Instantiate(projectile).gameObject;
            obj.SetActive(false);
            obj.GetComponent<Projectile>().Set(this, projectileLifeTime);
            return obj.GetComponent<Projectile>();
        });
    }
}
