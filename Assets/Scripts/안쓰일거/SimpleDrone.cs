﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleDrone : MonoBehaviour
{
    public Transform forwardPoint;
	public Transform upPoint;
    private Rigidbody2D body2D;
    public float pushPower = 100f;

    void Start()
    {
        body2D = GetComponent<Rigidbody2D>();

    }

    public void Push()
    {
		Debug.Log("푸쉬");
        Vector3 forward = (forwardPoint.position - transform.position).normalized;
		Debug.Log("포워드 : " + forward);
        body2D.AddForce(forward * pushPower);
    }
}
