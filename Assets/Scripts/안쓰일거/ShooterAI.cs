﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterAI : EnemyAI
{
    public Bullet Bullet;
    public Transform DetectPoint;
    public LayerMask DetectedTargetLayer;
    public float DetectRadius = 3.0f;

    public float DelayActionTime = 2;

    public float AttackMinDistance = 0.7f;

    private bool hasDetectedTarget = false;
    private Transform detectedTarget;
    private Vector2 detectedTargetDistance;

    private bool allowFire = true;
    private float fireCoolTime = 3f;
    private float fireTimer = 0.0f;

    void FixedUpdate()
    {
        if (HP <= 0) GameObject.Destroy(gameObject);

        // 탐지 레이더 작동
        if (!hasDetectedTarget)
        {
            Detect();
        }

        // 탐지한 타겟 제대로 찾는지 여부 확인하는 로그
        if (hasDetectedTarget)
        {
            // Debug.Log("Detect Collider: " + detectedTarget);
            UpdateTargetInfo();
            // 멈춘채로 공격	
            if (allowFire)
            {
                Debug.Log("멈춘채로 공격");
                Fire();
            }
        }
        fireTimer -= Time.deltaTime;
        if (fireTimer <= 0) allowFire = true;
    }

    public void Fire()
    {
        //Instantiate(Bullet, transform.position, Quaternion.identity);
        Bullet.Create(Bullet, this, transform, detectedTarget, Damage);
        fireTimer = fireCoolTime;
        allowFire = false;

    }

    private void SetDetectedTargetDistance()
    {
        if (detectedTarget != null)
        {
            detectedTargetDistance.x = transform.position.x - detectedTarget.position.x;
            detectedTargetDistance.y = transform.position.y - detectedTarget.position.y;
            //Debug.Log("DetectedTargetDistance: " + detectedTargetDistance.ToString());
        }
    }
    private void ReleaseDetectedTarget()
    {
        //Debug.Log("추적 타겟 해제");
        StopMove();
        detectedTarget = null;
        hasDetectedTarget = false;
    }

    private void Detect()
    {
        Collider2D detectedTargetCollider = Physics2D.OverlapCircle(DetectPoint.position, DetectRadius, DetectedTargetLayer);
        // Debug.Log("디텍트: " + detectedTargetCollider);
        if (detectedTargetCollider != null)
        {
            Debug.Log("추적 타겟 설정");
            detectedTarget = detectedTargetCollider.transform;
            hasDetectedTarget = true;
        }
    }

    protected void UpdateTargetInfo()
    {
        SetDetectedTargetDistance();
    }

    protected void StopMove()
    {
    }
}
