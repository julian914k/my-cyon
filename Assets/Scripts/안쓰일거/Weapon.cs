﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public ShooterLV1 lv1;
    public ShooterLV2 lv2;

    public int lv;

    public void Awake()
    {
        if (lv1 == null)
            lv1 = transform.Find("Shooter Lv 1").GetComponent<ShooterLV1>();
        if (lv2 == null)
            lv2 = transform.Find("Shooter Lv 2").GetComponent<ShooterLV2>();
    }

    public void Shoot()
    {
        switch(lv)
        {
            case 0:
                break;
            case 1:
                lv1.Shoot();
                break;
            case 2:
                lv2.Shoot();
                break;
        }
    }
}
