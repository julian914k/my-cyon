﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Projectile : PoolableObject
{
    private bool isShoot;
    private Vector3 dir;
    private float speed;

    public float lifeTime = 5f;
    public Shooter owner;

    public void Set(Shooter owner, float lifeTime = -1)
    {
        this.owner = owner;
        if (lifeTime >= 0)
            this.lifeTime = lifeTime;
    }

    private void Update()
    {
        if (isShoot)
        {
            transform.position += dir * speed * Time.deltaTime;
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Enemy"))
        {
            col.gameObject.SetActive(false);            
        }
    }

    public void Shoot(Vector3 dir, float speed)
    {
        isShoot = true;
        this.dir = dir;
        this.speed = speed;

        StartCoroutine(Inactive());
    }

    private IEnumerator Inactive()
    {
        yield return new WaitForSeconds(lifeTime);

        owner.pool.Push(this);
    }
}
