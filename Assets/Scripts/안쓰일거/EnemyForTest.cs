﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyForTest : MonoBehaviour
{
    public Transform targetPoint;
    public float speed = 2;

    public float lifeTime = 10;

    private Vector3 dir;

    private void Start()
    {
        dir = (targetPoint.position - transform.position).normalized;
        StartCoroutine(Die());
    }

    private void Update()
    {
        transform.position += dir * speed * Time.deltaTime;
    }

    private IEnumerator Die()
    {
        yield return new WaitForSeconds(lifeTime);

        Destroy(gameObject);
    }
}
