﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterLV1 : Shooter
{
    public Drone drone;
    public float usedFuelAmount = 5;

    private Vector3 right;
    private Vector3 left;

    public Transform rightPoint;
    public Transform leftPoint;

    public float fuelCost = 5;
    public bool isShootable;
    public float delay = 0.5f;
    //private bool shootable;

    private void Awake()
    {
        Allocate();

        isShootable = true;
        //shootable = true;
    }

    private void Update()
    {
        Vector3 forward = (forwardPoint.position - transform.position).normalized;
        right = new Vector3(forward.y, -forward.x, 0);
        left = new Vector3(-right.x, right.y, right.z);
    }

    public void Shoot()
    {
        if (isShootable)
        {
            Projectile a = pool.Pop();
            Projectile b = pool.Pop();
            a.transform.position = leftPoint.position;
            b.transform.position = rightPoint.position;
            a.Shoot(left, shootSpeed);
            b.Shoot(right, shootSpeed);

            drone.UseFuel(fuelCost);

            isShootable = false;
            StartCoroutine(Delay());
        }
    }

    private IEnumerator Delay()
    {
        yield return new WaitForSeconds(delay);

        isShootable = true;
        // shootable = true;
    }
}
