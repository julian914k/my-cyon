﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(CircleCollider2D))]
public class ShooterLV2 : Shooter
{
    private List<Transform> targets;

    public Drone drone;
    public float delay = 0.5f;
    public float fuelCost = 5;
    public bool isShootable;

    private void Awake()
    {
        Allocate();

        targets = new List<Transform>();

        isShootable = true;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Enemy"))
        {
            if (!targets.Contains(col.transform))
                targets.Add(col.transform);
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.CompareTag("Enemy"))
        {
            if (targets.Contains(col.transform))
                targets.Remove(col.transform);
        }
    }

    public void Shoot()
    {
        if (isShootable && targets.Any())
        {
            isShootable = false;
            targets.Sort((a, b) =>
            {
                float disA = (a.transform.position - transform.position).sqrMagnitude;
                float disB = (b.transform.position - transform.position).sqrMagnitude;
                return disA.CompareTo(disB);
            });
            GameObject target = targets[0].gameObject;
            Projectile p = pool.Pop();
            p.Shoot((target.transform.position - transform.position).normalized, shootSpeed);

            drone.UseFuel(fuelCost);

            //isShootable = false;
            //Projectile projectile = pool.Pop();
            //projectile.transform.position = transform.position;
            //projectile.Shoot((target.transform.position - transform.position).normalized, shootSpeed);
            StartCoroutine(Delay());
        }
    }

    private IEnumerator Delay()
    {
        yield return new WaitForSeconds(delay);

        isShootable = true;
    }
}
