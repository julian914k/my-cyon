﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkyChange : MonoBehaviour
{
    public Sprite[] sprites;

    public SpriteRenderer spriteRenderer;

    public Basecamp basecamp;

    private int prevLevel;

    public void Update()
    {
        switch(basecamp.BasecampLevel)
        {
            case 0:
                if(spriteRenderer.sprite != sprites[0])
                    spriteRenderer.sprite = sprites[0];
                break;
            case 1:
                if (spriteRenderer.sprite != sprites[1])
                    spriteRenderer.sprite = sprites[1];
                break;
            case 2:
                if (spriteRenderer.sprite != sprites[2])
                    spriteRenderer.sprite = sprites[2];
                break;
        }
    }
}
