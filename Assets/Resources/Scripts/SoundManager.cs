﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{

    //협업 작업
    //스테이지 변수 갱신 필요
    //드론 기지 착륙, 이륙시 변수 갱신 필요

    public AudioSource[] BGM;
    public AudioSource[] SFX;
    public int currentStage;
    bool isDroneOnBase;
    // 0 = 타이틀&메인 메뉴, 1~3 = 스테이지 1~3

    // Use this for initialization
    void Start()
    {
        EnterStage(1);
    }

    void Update()
    {

    }

    // NOTE : 게임 시작 시 불러주세요
    public void TitleStart()
    {
        // 게임 첫 시작 시 타이틀 음악 재생(루프)
        BGM[6].Play();

    }

    // NOTE : 메인 메뉴 진입시 불러주세요
    public void EnterMainMenu()
    {
        // 메인 메뉴 진입 시 타이틀 음악 재생(루프) & 모든 다른 브금 중단
        if (BGM[6].isPlaying == true)
        { }
        else
        {
            for (int i = 0; i < 9; i++)
            {
                BGM[i].Stop();

            }
            BGM[6].Play();
        }

    }

    // NOTE : 스테이지에 진입할 때 불러주세요
    public void EnterStage(int StageNum)
    {

        // 음악 올스탑
        for (int i = 0; i < 9; i++)
        {
            BGM[i].Stop();

        }

        // 스테이지 시작 사운드 플레이
        BGM[7].Play();
        SFX[13].Play();
        switch (StageNum)
        {
            case 1:
                BGM[0].Play();
                break;

            case 2:

                BGM[2].Play();

                break;

            case 3:
                BGM[4].Play();
                break;
        }
    }


    // NOTE : 2단계에 진입할 때 불러주세요
    public void EnterBaseLv2(int StageNum)
    {
        switch (StageNum)
        {
            case 1:
                BGM[0].Pause();
                BGM[1].Play();
                break;

            case 2:
                BGM[2].Pause();
                BGM[3].Play();

                break;

            case 3:
                BGM[4].Pause();
                BGM[5].Play();
                break;
        }
    }

    public void DroneOnBase()
    {
        BGM[8].Play();
    }

    public void DroneOutBase()
    {
        BGM[8].Pause();
    }




}









